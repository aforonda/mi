window.addEventListener("load", iniciar);

var verificar = true;
var verificarUser = true;
var verificarEmail = true;
var verificarPass = true;
var verificarMovil = true;

function iniciar() {
    nombre = document.getElementById("user");
    email = document.getElementById("email");
    password = document.getElementById("psw");
    movil = document.getElementById("movil");
    boton = document.getElementById("registro");
    boton.addEventListener("click", validacion);
    expUser = /^[a-zA-ZÑñÁáÉéíÍóÓúÚüñU\s]+$/;
    expEmail = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/;
    expPsw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;
    expMovil = /^[6,7][0-9]{8}$/;
    validarUser();
    validarEmail();
    validarPass();
    validarMovil();
}

function validarUser() {
    nombre = document.getElementById("user");
    nombre.oninput = function() {
        if (!nombre.value) {
            nombre.className='form-input';
        }
        else if (!expUser.exec(nombre.value)) {
        nombre.className='error';
        verificarUser = false;
        }
        else {
            nombre.className='form-input';
            verificarUser = true;
        }
    };
}

function validarEmail() {
    email = document.getElementById("email");
    email.oninput = function() {
        if (!email.value) {
            email.className='form-input';
        }
        else if (!expEmail.exec(email.value)) {
        email.className='error';
        verificarEmail = false;
        }
        else {
            email.className='form-input';
            verificarEmail = true;
        }
    };
}

function validarPass() {
    password = document.getElementById("psw");
    password.oninput = function() {
        if (!password.value) {
            password.className='form-input';
        }
        else if (!expPsw.exec(password.value)) {
        password.className='error';
        verificarPass = false;
        }
        else {
            password.className='form-input';
            verificarPass = true;
        }
    };
}

function validarMovil() {
    movil = document.getElementById("movil");
    movil.oninput = function() {
        if (!movil.value) {
            movil.className='form-input';
        }
        else if (!expMovil.exec(movil.value)) {
        movil.className='error';
        verificarMovil = false;
        }
        else {
            movil.className='form-input';
            verificarMovil = true;
        }
    };
}

function validacion() {
    verificar=true;
    nombre = document.getElementById("user");
    email = document.getElementById("email");
    password = document.getElementById("psw");
    movil = document.getElementById("movil");
    
    if (!nombre.value) {
        verificar = false;    
        alert("Debes introducir un nombre");
    }   
    else if (!email.value) {
        verificar = false;    
        alert("Debes introducir un email");
    }   
    else if (!password.value) {
        verificar = false;  
        alert("Debes introducir una contraseña");
    }
    else if (!movil.value) {
        verificar = false;   
        alert("Debes introducir un número de móvil");
    }
    if (verificar && verificarUser && verificarEmail
            && verificarPass && verificarMovil) {
        alert("Registro con exito");
        document.registrarse.submit();
        location.href = "login.html";
        añadir();
    }
    else if(verificarUser===false){
        alert("Nombre de usuario incorrecto");
    }
    else if(verificarEmail===false){
        alert("Email incorrecto");
    }
    else if(verificarPass===false){
        alert("Contraseña incorrecta, debe de tener minimo 6 caracteres y entre ellos minimo un numero y una mayuscula");
    }
    else if(verificarMovil===false){
        alert("Telefono incorrecto, debe tener 9 numeros y empezar por 6 o 7");
    }
}
