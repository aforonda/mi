/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

var dataBase = null;


window.addEventListener("load", iniciarbotonMod);

function iniciarbotonMod() {
    botona = document.getElementById("modificarArt");
    botona.addEventListener("click", confirmarModificar);
}


function confirmarModificar() {
    var active = dataBase.result;
    var data = active.transaction(["datosArticulos"], "readwrite");
    var object = data.objectStore("datosArticulos");
    var NombreArti = document.getElementById("NombreArti").value;
    var descripc = document.getElementById("descripc").value;
    var precios = document.getElementById("precios").value;
    var categoriaArti = document.getElementById("categoriaArti");
    var numid = localStorage.getItem("idArt");
    var elements = [];
    var id = parseInt(numid);

    object.openCursor().onsuccess = function (e) {
        var result = e.target.result;
        if (result === null) {
            return;
        }
        elements.push(result.value);
        result.continue();

        for (var key in elements) {
            if (elements[key].id === id) {
                var updateData = result.value;
                updateData.nomArticulo = NombreArti;
                updateData.descripcion = descripc;
                updateData.precio = precios;
                updateData.categoria = categoriaArti.value;
                object.put(updateData);

            }
            elements = [];
        }
        data.oncomplete = function (e) {
            location.reload();
            document.querySelector("#NombreArti").value = '';
            document.querySelector("#descripc").value = '';
            document.querySelector("#precios").value = '';
            document.querySelector("#categoriaArti").value = '';
        };
    };
}

