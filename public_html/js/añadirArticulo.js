/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

var dataBase = null;

window.addEventListener("load", iniciarBotonArt);

function iniciarBotonArt() {
    boton = document.getElementById("validarArt");
    window.addEventListener("load", drope);
    boton.addEventListener("click",finalizar);
}

function finalizar(){
    if(document.querySelector("#NombreArt").value === '' || document.querySelector("#descrip").value === '' 
            || document.querySelector("#precio").value === '' || document.querySelector("#categoriaArt").value === '') {
        alert("Rellene todos los campos");
    }
    else{
        alert("Articulo agregado recarga la pagina"); 
    location.href="tusArticulos.html";
    }
    
}

function drope(ev) {

    if (document.querySelector("#NombreArt").value === '' || document.querySelector("#descrip").value === '' || document.querySelector("#precio").value === '' || document.querySelector("#categoriaArt").value === '') {
        ev.preventDefault();
        var arch1 = new FileReader();
        arch1.addEventListener('load', leerFotArt, false);
        arch1.readAsDataURL(ev.dataTransfer.files[0]);

    } else {
        ev.preventDefault();
        var arch1 = new FileReader();
        arch1.addEventListener('load', leerFotArt, false);
        arch1.onload = function (fileLoadedEvent) {
            var elements = [];
            var active = dataBase.result;
            var data = active.transaction(["datosArticulos"], "readwrite");
            var object = data.objectStore("datosArticulos");
            var srcData = fileLoadedEvent.target.result; // <--- data: base64

            object.openCursor().onsuccess = function (e) {
                var result = e.target.result;

                var result = object.put({
                    nomArticulo: document.querySelector("#NombreArt").value,
                    descripcion: document.querySelector("#descrip").value,
                    precio: document.querySelector("#precio").value,
                    categoria: document.querySelector("#categoriaArt").value,
                    pEmail: sessionStorage.email,
                    foto: srcData
                });   
            };

        };
        arch1.readAsDataURL(ev.dataTransfer.files[0]);
    }

}

function allowDrope(ev)
{
    ev.preventDefault();
}

function leerFotArt(ev) {
    document.getElementById('fotoArticulo').style.backgroundSize = "100% 100%";
    document.getElementById('fotoArticulo').style.opacity = "1";
    document.getElementById('fotoArticulo').style.backgroundImage = "url('" + ev.target.result + "')";
}
